/**
 * 現在の西暦年を取得して著作権表示する。
 */
function displayCopyright() {
	var date = new Date();
	var year = date.getFullYear();
	var copyright_template = "(C) 2009-:CURRENT_YEAR MURA Yoshiko. All Rights Reserved.";
	var copyright = copyright_template.replace(/:CURRENT_YEAR/, year);
	document.write(copyright);
}
